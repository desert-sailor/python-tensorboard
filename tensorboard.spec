%global _empty_manifest_terminate_build 0
Name:		python-tensorboard
Version:	2.12.1
Release:	2
Summary:	TensorBoard lets you watch Tensors Flow
License:	Apache 2.0
URL:		https://github.com/tensorflow/tensorboard
Source0:	https://github.com/tensorflow/tensorboard/archive/refs/tags/%{version}.tar.gz
Source1:	https://files.pythonhosted.org/packages/da/61/6e9ff8258422d287eec718872fb71e05324356722ab658c8afda25f51539/tensorboard_data_server-0.7.1-py3-none-any.whl
Source2:	bazel-external.tar.gzaa
Source3:	bazel-external.tar.gzab
Source4:	bazel-external.tar.gzac
Source5:	bazel-execroot.tar.gzaa
Source6:	bazel-arm-adds.tar.gzaa
Source7:	bazel-arm-adds.tar.gzab
Source8:	adapt-external-for-aarch64.patch

Requires:	python3-absl-py
Requires:	python3-grpcio
Requires:	python3-google-auth
Requires:	python3-google-auth-oauthlib
Requires:	python3-markdown
Requires:	python3-numpy
Requires:	python3-protobuf
Requires:	python3-requests
Requires:	python3-setuptools
Requires:	python3-tensorboard-data-server
Requires:	python3-tensorboard-plugin-wit
Requires:	python3-werkzeug
Requires:	python3-wheel

%description
TensorBoard is a suite of web applications for inspecting and understanding
your TensorFlow runs and graphs.

Releases prior to 1.6.0 were published under the ``tensorflow-tensorboard`` name
and may be found at https://pypi.python.org/pypi/tensorflow-tensorboard.

%package -n python3-tensorboard
Summary:	TensorBoard lets you watch Tensors Flow
Provides:	python-tensorboard
BuildRequires:	python3-devel
BuildRequires:	python3-pip
BuildRequires:	python3-setuptools
BuildRequires:	python3-wheel
BuildRequires:	python3-packaging
BuildRequires:  python3-numpy
BuildRequires:	python3-absl-py
BuildRequires:	python3-virtualenv
BuildRequires:	python3-google-api-client
BuildRequires:  bazel == 5.3.0 gcc gcc-c++
%description -n python3-tensorboard
TensorBoard is a suite of web applications for inspecting and understanding
your TensorFlow runs and graphs.

Releases prior to 1.6.0 were published under the ``tensorflow-tensorboard`` name
and may be found at https://pypi.python.org/pypi/tensorflow-tensorboard.

%package -n python3-tensorboard-data-server
Summary:	Fast data loading for TensorBoard
Provides:	python3-tensorboard-data-server
BuildRequires:	python3-devel
BuildRequires:	python3-pip
BuildRequires:	python3-setuptools
BuildRequires:	python3-wheel
%description -n python3-tensorboard-data-server
Fast data loading for TensorBoard.

%package help
Summary:	Development documents and examples for tensorboard
Provides:	python3-tensorboard-doc
%description help
TensorBoard is a suite of web applications for inspecting and understanding
your TensorFlow runs and graphs.

Releases prior to 1.6.0 were published under the ``tensorflow-tensorboard`` name
and may be found at https://pypi.python.org/pypi/tensorflow-tensorboard.

%prep
%autosetup -p1 -n tensorboard-%{version} -b 0
extdir=$(bazel --output_user_root=`pwd`/../output_user_root info output_base)
srcdir=`pwd`
homeuser=`whoami`
mkdir -p ${extdir}
cat %{SOURCE2} %{SOURCE3} %{SOURCE4} > bazel-external.tar.gz
tar xzvf bazel-external.tar.gz -C ${extdir}
cat %{SOURCE5} > bazel-execroot.tar.gz
tar xzvf bazel-execroot.tar.gz -C ${extdir}
%ifarch aarch64
patch -d ${extdir}/external -p1 < %{SOURCE8}
cat %{SOURCE6} %{SOURCE7}  > adds-aarch64-cache.tar.gz
tar xzvf adds-aarch64-cache.tar.gz -C ${extdir}
%endif
# relinks adpapt to ebs&obs
rm -rf ${extdir}/external/platforms
ln -sfn $(find ${extdir}/../install -name embedded_tools) ${extdir}/external/bazel_tools
ln -sfn $(find ${extdir}/../install -maxdepth 2 -name platforms) ${extdir}/external/platforms
ln -sfn $(find /usr/lib/jvm -maxdepth 1 -name java-11-openjdk)/bin	${extdir}/external/local_jdk/bin
ln -sfn $(find /usr/lib/jvm -maxdepth 1 -name java-11-openjdk)/conf	${extdir}/external/local_jdk/conf
ln -sfn $(find /usr/lib/jvm -maxdepth 1 -name java-11-openjdk)/include	${extdir}/external/local_jdk/include
ln -sfn $(find /usr/lib/jvm -maxdepth 1 -name java-11-openjdk)/legal	${extdir}/external/local_jdk/legal
ln -sfn $(find /usr/lib/jvm -maxdepth 1 -name java-11-openjdk)/lib	${extdir}/external/local_jdk/lib
ln -sfn $(find /usr/lib/jvm -maxdepth 1 -name java-11-openjdk)/release	${extdir}/external/local_jdk/release
ln -sfn $(find /usr/lib/jvm -maxdepth 1 -name java-11-openjdk)/tapset	${extdir}/external/local_jdk/tapset
for f in $(find $extdir -lname "*/f1a05a074786842b07afcf52636c4445/external/*"); do OLDLINK=$(readlink $f); echo $OLDLINK; NEWLINK=${extdir}/external${OLDLINK#*external}; LINKTP=$([ ! -d $NEWLINK ] && echo -n "-sf" || echo -n "-sfn"); ln $LINKTP $NEWLINK $f-newlink; mv -Tf $f-newlink $f; done
for f in $(find $extdir -lname "*/f1a05a074786842b07afcf52636c4445/execroot/*"); do OLDLINK=$(readlink $f); echo $OLDLINK; NEWLINK=${extdir}/execroot${OLDLINK#*execroot}; LINKTP=$([ ! -d $NEWLINK ] && echo -n "-sf" || echo -n "-sfn"); ln $LINKTP $NEWLINK $f-newlink; mv -Tf $f-newlink $f; done
for f in $(find $extdir -lname "/root/rpmbuild/*"); do OLDLINK=$(readlink $f); echo $OLDLINK; NEWLINK=${HOME}/rpmbuild${OLDLINK#*rpmbuild}; LINKTP=$([ ! -d $NEWLINK ] && echo -n "-sf" || echo -n "-sfn"); ln $LINKTP $NEWLINK $f-newlink; mv -Tf $f-newlink $f; done
# Update local path to ci path for ebs&obs
oldext='/root/rpmbuild/BUILD/output_user_root/f1a05a074786842b07afcf52636c4445'
oldsrc='/root/rpmbuild/BUILD/tensorboard-2.12.1'
sed -i s#${oldext}#${extdir}#g `grep ${oldext} -rl ${extdir}/external`
sed -i s#${oldext}#${extdir}#g `grep ${oldext} -rl ${extdir}/execroot`
sed -i s#${oldsrc}#${srcdir}#g `grep ${oldsrc} -rl ${extdir}/external`
sed -i s#${oldsrc}#${srcdir}#g `grep ${oldsrc} -rl ${extdir}/execroot`
# Update yarn sum for user path change. Get the value of oldyarn: 
#      grep -r "FILE:@yarn//:bin/yarn" ./external 
oldyarn='4db35273dd0456e9cc171ebc9d3857b4edeb7a16d4c76c5eea6383ccc7062284'
%ifarch aarch64
oldyarn='2206ebdaafbb5b7a637d9f4584083c79c49593fca902d9f303ac6b86bfaa654b'
%endif
yarnsum=$(sha256sum ${extdir}/external/yarn/bin/yarn)
newyarn=${yarnsum%%%% *}
sed -i s#${oldyarn}#${newyarn}#g `grep ${oldyarn} -rl ${extdir}/external`
# Update local config cc&sh for user path change
sed -i "s#^ENV\:USER.*#ENV\:USER ${homeuser}#g" ${extdir}/external/@local_config_cc.marker
sed -i "s#^ENV\:PATH.*#ENV\:PATH ${PATH}#g" ${extdir}/external/@local_config_sh.marker

%build
mkdir dist
cp %{SOURCE1} dist
bazel --output_user_root=`pwd`/../output_user_root build //tensorboard/pip_package:build_pip_package  --verbose_failures --experimental_local_memory_estimate --jobs=16
bazel --output_user_root=`pwd`/../output_user_root run //tensorboard/pip_package:extract_pip_package -- `pwd`/dist

%install
%py3_install_wheel *
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

%files -n python3-tensorboard
%{_bindir}/tensorboard
%{python3_sitelib}/tensorboard
%{python3_sitelib}/tensorboard-*

%files -n python3-tensorboard-data-server
%{python3_sitelib}/tensorboard_data_server
%{python3_sitelib}/tensorboard_data_server-*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Sep 11 2023 Dongxing Wang <dxwangk@isoftstone.com> - 2.12.1-2
- upgrade bazel to version 5.3.0

* Tue Sep 5 2023 Dongxing Wang <dxwangk@isoftstone.com> - 2.12.1-1
- upgrade to version 2.12.1

* Fri Jul 21 2023 zhuofeng <zhuofeng2@huawei.com> - 2.12.0-1
- Update package to version 2.12.0

* Sat May 06 2023 Dongxing Wang <dxwangk@isoftstone.com> - 2.10.1-2
- change depends google-auth

* Thu Nov 10 2022 Bin Hu <hubin73@huawei.com> - 2.10.1-1
- upgrade to version 2.10.1

* Thu Oct 08 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
